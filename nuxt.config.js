export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: 'universal',
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'static',
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    title: 'My Awesome Nuxt Blog | Made with Nuxt JS',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'This is a nuxt js blog, built with Nuxt JS.',
      },
      // Open Graph/Facebook
      {
        hid: 'og:type',
        name: 'og:type',
        content: 'website',
      },
      {
        hid: 'og:url',
        name: 'og:url',
        content: 'https://my-awesome-nuxt-blog.netlify.app/',
      },
      {
        hid: 'og:title',
        name: 'og:title',
        content: 'My Awesome Nuxt Blog | Made with Nuxt JS',
      },
      {
        hid: 'og:description',
        name: 'og:description',
        content: 'This is a nuxt js blog, built with Nuxt JS.',
      },
      {
        hid: 'og:image',
        name: 'og:image',
        content:
          'https://my-awesome-nuxt-blog.netlify.app/my-awesome-nuxt-blog.png',
      },
      // Twiter cards
      {
        hid: 'twitter:card',
        name: 'twitter:card',
        content: 'summary_large_image',
      },
      {
        hid: 'twitter:url',
        name: 'twitter:url',
        content: 'https://my-awesome-nuxt-blog.netlify.app/',
      },
      {
        hid: 'twitter:title',
        name: 'twitter:title',
        content: 'My Awesome Nuxt Blog | Made with Nuxt JS',
      },
      {
        hid: 'twitter:description',
        name: 'twitter:description',
        content: 'This is a nuxt js blog, built with Nuxt JS.',
      },
      {
        hid: 'twitter:image',
        name: 'twitter:image',
        content:
          'https://my-awesome-nuxt-blog.netlify.app/my-awesome-nuxt-blog.png',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      // {
      //   rel: 'stylesheet',
      //   href:
      //     'https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap',
      // },
    ],
  },
  /*
   ** Global CSS
   */
  css: ['assets/styles.css'],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: ['~/plugins/site-config.js', '~/plugins/filters.js'],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {},
  /*
   ** Custom Progress Bar
   */
  loading: true,

  env: {
    fsApiKey:
      process.env.FS_API_KEY || 'AIzaSyCt7rGO8RRL6cJyyQ0K6Ytp5MZJkoCPmHc',
  },
}
