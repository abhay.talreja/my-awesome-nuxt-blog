export default {
  siteName: 'My Awesome Nuxt Blog',
  siteSubtitle: 'Learn how to create this blog',
  mainMenu: [
    {
      name: 'Home',
      link: '/',
    },
    {
      name: 'Blog',
      link: '/posts',
    },
    {
      name: 'About',
      link: '/about',
    },
    {
      name: 'Create Post',
      link: '/posts/create',
    },
  ],
}
