export const state = () => ({
  token: null,
})

export const mutations = {
  setToken: (state, token) => {
    state.token = token
  },
  clearToken: (state) => {
    state.token = null
  },
}

export const actions = {
  logout({ commit }) {
    localStorage.setItem('token', null)
    localStorage.setItem('expirationDate', null)
    return commit('clearToken')
  },
  authenticate({ commit }, user) {
    const postUrl = user.isSignUp
      ? 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key='
      : 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key='

    // eslint-disable-next-line no-console
    console.log(user)
    const firebaseUser = {
      email: user.email,
      password: user.password,
      returnSecureToken: true,
    }
    return this.$axios
      .post(postUrl + process.env.fsApiKey, firebaseUser)
      .then((result) => {
        // eslint-disable-next-line no-console
        console.log('Firebase User Created', result)
        const token = result.data.idToken
        const expiresIn = result.data.expiresIn
        commit('setToken', token)
        localStorage.setItem('token', token)
        localStorage.setItem(
          'expirationDate',
          new Date().getTime() + Number.parseInt(expiresIn) * 1000
        )
      })
      .catch((e) => {
        // eslint-disable-next-line no-console
        console.log(e.response.data)
        return { isError: true, message: e.response.data.error.message }
      })
  },
  validateAuth({ commit, dispatch }) {
    if (process.client) {
      const token =
        localStorage.getItem('token') === 'null'
          ? null
          : localStorage.getItem('token')
      const expirationDate = localStorage.getItem('expirationDate')
      if (new Date().getTime() > +expirationDate) {
        dispatch('logout')
        return
      }
      commit('setToken', token)
    }
  },
}

export const getters = {}
