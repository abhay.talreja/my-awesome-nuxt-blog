import FireStoreParser from 'firestore-parser'

export const state = () => ({
  loadedPosts: [],
})

export const mutations = {
  setPosts: (state, posts) => {
    state.loadedPosts = posts
  },
  addPost: (state, post) => {
    state.loadedPosts.push(post)
  },
  editPost: (state, editedPost) => {
    // eslint-disable-next-line no-console
    console.log('edit post called', editedPost)
    const postIndex = state.loadedPosts.findIndex(
      (post) => post.id === editedPost.id
    )
    state.loadedPosts[postIndex] = editedPost
  },
}

export const actions = {
  setPosts({ commit }, posts) {
    commit('setPosts', posts)
  },
  addPost({ commit, rootState }, post) {
    const config = {
      headers: {
        Authorization: 'Bearer ' + rootState.auth.token,
      },
    }
    // Send Post to Firebase
    const newPost = {
      fields: {
        content: {
          stringValue: post.content,
        },
        title: {
          stringValue: post.title,
        },
        thumbnail: {
          stringValue: post.thumbnail,
        },
        slug: {
          stringValue: post.slug,
        },
        previewText: {
          stringValue: post.previewText,
        },
        author: {
          stringValue: post.author,
        },
        updatedDate: {
          timestampValue: new Date(),
        },
      },
    }
    return this.$axios
      .post(
        'https://firestore.googleapis.com/v1/projects/my-awesome-nuxt-project/databases/(default)/documents/posts',
        newPost,
        config
      )
      .then((result) => {
        // eslint-disable-next-line no-console
        console.log(result)
        commit('addPost', post)
      })
  },
  editPost({ commit, rootState }, post) {
    const config = {
      headers: {
        Authorization: 'Bearer ' + rootState.auth.token,
      },
    }
    // Send Post to Firebase
    const editedPost = {
      fields: {
        content: {
          stringValue: post.content,
        },
        title: {
          stringValue: post.title,
        },
        thumbnail: {
          stringValue: post.thumbnail,
        },
        slug: {
          stringValue: post.slug,
        },
        previewText: {
          stringValue: post.previewText,
        },
        author: {
          stringValue: post.author,
        },
        updatedDate: {
          timestampValue: new Date(),
        },
      },
    }
    return this.$axios
      .patch(
        'https://firestore.googleapis.com/v1/projects/my-awesome-nuxt-project/databases/(default)/documents/posts/' +
          post.id,
        editedPost,
        config
      )
      .then((result) => {
        // eslint-disable-next-line no-console
        console.log(result)
        const data = FireStoreParser(result.data)
        const firebaseUpdatedPost = {
          ...data.fields,
          id: data.name.substring(data.name.lastIndexOf('/') + 1),
        }
        commit('editPost', firebaseUpdatedPost)
      })
  },
}

export const getters = {
  getPosts: (state) => {
    return state.loadedPosts
  },
}
