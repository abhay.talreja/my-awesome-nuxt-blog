import FireStoreParser from 'firestore-parser'

export const actions = {
  nuxtServerInit(vuexContext, context) {
    return context.app.$axios
      .get(
        'https://firestore.googleapis.com/v1/projects/my-awesome-nuxt-project/databases/(default)/documents/posts?orderBy=updatedDate desc'
      )
      .then((result) => {
        const data = result.data.documents
        const postsObjects = FireStoreParser(data)
        const postArray = []

        // eslint-disable-next-line no-console
        for (const key in postsObjects) {
          postArray.push({
            ...postsObjects[key].fields,
            id: postsObjects[key].name.substring(
              postsObjects[key].name.lastIndexOf('/') + 1
            ),
          })
        }

        vuexContext.commit('posts/setPosts', postArray)
      })
      .catch((e) => {
        // eslint-disable-next-line no-console
        console.error(e)
      })
  },
}
