import SiteConfig from '~/config/site-config'

export default ({ app }, inject) => {
  inject('siteConfig', SiteConfig)
}
