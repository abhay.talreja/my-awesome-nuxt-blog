import Vue from 'vue'

export function timeAgo(time) {
  const now = Date.now()
  //   const createdDate = new Date('07/16/2020')
  const between = now / 1000 - new Date(time) / 1000

  if (between < 3600) {
    // minutes
    return plurarlize(~~(between / 60), ' minute')
  } else if (between < 86400) {
    // hours
    return plurarlize(~~(between / 3600), ' hour')
  } else {
    // days
    return plurarlize(~~(between / 86400), ' day')
  }
}

function plurarlize(time, label) {
  if (time === 1) {
    return time + label
  }
  return time + label + 's'
}

const filters = {
  timeAgo,
}

export default filters

Object.keys(filters).forEach((key) => {
  Vue.filter(key, filters[key])
})
