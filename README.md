# Learn Nuxt JS full Course for Free

I started this course and recorded 95% of the course before I had a personal life changing event. I couldn't drive it to conclusion but I wanted to share the material with everyone cause there are lot of good tips, tricks and techniques you will need in your Nuxt JS project. If it is helpful, if you wish to connect with me, to discuss any additional topics or want me to make a course on a new framework. Please reach out to me at one of the below forums.

Happy Learning!

~Abhay Talreja

[Youtube](https://www.youtube.com/user/abhaytalreja?sub_confirmation=1) | [LinkedIn](https://www.linkedin.com/in/abhaytalreja)

[![Nuxt Js - Full Course](https://img.youtube.com/vi/YOUTUBE_VIDEO_ID_HERE/0.jpg)](https://www.youtube.com/watch?v=YOUTUBE_VIDEO_ID_HERE)

<GitLab Link>


**Course Curriculum:**

## Section 1 - Your First Nuxt JS App

### Pre-requisites:

- Nuxt JS - https://nuxtjs.org/

- VS Code - https://code.visualstudio.com/

- Node JS - https://nodejs.org/en/

- Npm JS - https://www.npmjs.com/

- Npx - https://www.npmjs.com/package/npx Use npm install -g npx

Here are the links for the important items we discussed in this section

- Netlify - http://netlfiy.com/

- Gitlab - http://gitlab.com/

- Nuxt Installation - https://nuxtjs.org/guide/installation

- Nuxt JS Docs: Routing - https://nuxtjs.org/guide/routing

**Our Deployed Nelify link - https://my-next-app.netlify.app/ (This app link will have all the content for this app, it may include code from future lectures)**

## Section 2 - Views in Nuxt JS

Here are the important links for the lecture

### Commits

- Nested Routes in Nuxt JS - https://gitlab.com/abhay.talreja/my-next-app/-/commit/b598844ab29690efb6374e42a095e134f927bcc8

- Validate - https://gitlab.com/abhay.talreja/my-next-app/-/commit/93881082dba37fe6386064f4b278b479557654b6

- Layouts - https://gitlab.com/abhay.talreja/my-next-app/-/commit/ec35009e295898360d54d79808d949ca7d431fb6

- Custom Error Page - https://gitlab.com/abhay.talreja/my-next-app/-/commit/7766b25955862828ca48a4dbac94839cc2fe2c83

- Custom Components - https://gitlab.com/abhay.talreja/my-next-app/-/commit/078393ce5582214cae33fb591c022c2b12337a0b

### Reference Links

- Nuxt Child - https://nuxtjs.org/api/components-nuxt-child

- Routing - https://nuxtjs.org/guide/routing

- Validate - https://nuxtjs.org/api/pages-validate

- Layout - https://nuxtjs.org/api/pages-layout

- Error Page - https://nuxtjs.org/guide/views#error-page

- Components Property - https://nuxtjs.org/api/configuration-components

- Components in Vue - https://vuejs.org/v2/guide/components.html

## Section 3 - Real World Application (Part 1)

Our Deployed Nuxt Blog - https://my-awesome-nuxt-blog.netlify.app/

> P.S - Our Deployed Nuxt Blog will have all the changes from the upcoming lectures too. So, when you see it, you may see the final version of the app.

### Important Links

- Google Fonts - https://fonts.google.com/

- Roboto Font - https://fonts.google.com/specimen/Roboto

### Commits

- Installing a New Project, Adding Git and Deploying - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/3f9376c257226ac0d7ce41d289b2fb7a8b7550a2

- Blog Skeleton - Setting up the links - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/917412ba6e6c71b9770ce693ee4f389922f7c9f3

- Creating Post Lists - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/94e06a415a92a8db4327d1b72657e506d630a774

- Creating Individual Post Page - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/c2591c9f77e6ac58308e3dfc3638716fc7670e2c

- Styling Post and Post List Pages - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/39def3daae21804e0827747d71198b976d1a4a98

- Adding Custom Fonts and Global Stylesheets - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/9d15a0d0ba72e066ba89bedc277d277a154705a1

- Creating a PostItem for every Post - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/8e768c8fac309380aba5e1665c3ea3f5b7430d18

- Adding Site Hero - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/ec9ef3aad189218ba48d34e4960d13a8f4e484ff

- Adding Call To Action Button on the Blog - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/11dd825fb831ca5b7651eaf7af8c216fa391e0d5

- Defect Fix for the App Button - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/47eb86c3ffc80aa04f455795c7133dbef6f72150

- Adding a Site Header - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/cb7d2ae9416584a8409e3201ce5ea14e886d2d6c

- Creating a Plugin for our Site Config - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/2f67bdae7a87cabf12acdc66b6990c434199559e

- Styling the Site Nav - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/24bf0aa1e3ad310e856033f14381764044858792

- Adding Post List to the Blog Page - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/f7e8bc21521e3c43b6589258cfba76d1e679b70e

- Creating a Post Form for Input - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/dcba646d7e19b07ff565cb9321ac543ced4fc354

- Styling our Create Post Page - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/dcba646d7e19b07ff565cb9321ac543ced4fc354

- Making the Content field a TextArea - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/dcba646d7e19b07ff565cb9321ac543ced4fc354

- Creating the Edit Form for the Posts - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/dcba646d7e19b07ff565cb9321ac543ced4fc354

- Adding Admin (Login/Sign up) Page - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/f764f9f10c7f1ffe839dfce720e1422c0d7e7bf2

## Section 4 - Real World Application - Setting up Vuex Store for our application

Our Deployed Nuxt Blog - https://my-awesome-nuxt-blog.netlify.app/

> P.S - Our Deployed Nuxt Blog will have all the changes from the upcoming lectures too. So, when you see it, you may see the final version of the app.

### Important Links

- Vuex Guide - https://vuex.vuejs.org/guide/

- Vue Dev Tools - https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd?hl=en

### Commits

- Understanding asyncData - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/20bf37ae6181cd5613b41d819174eed1ebac9ad6

- AsyncData on Client v/s Server - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/21adc16770fed7e3b48e02ef22de43aec0e8354e

- Using Async Data for Single Post Page - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/5e37fd98f70d9b43d4f3e879bbb91ab1424ce6c8

- Setting up our Vuex Store for the loaded Posts - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/9e7863ffc4cb0d77c9e5ae4b1653aa9a8f89a990

- NuxtServerInit - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/e8c89a73cba4ea7f79e4c1095e3a4dc60a2042ca

## Section 5 - Setting up and Using our Firestore Database

Our Deployed Nuxt Blog - https://my-awesome-nuxt-blog.netlify.app/

> P.S - Our Deployed Nuxt Blog will have all the changes from the upcoming lectures too. So, when you see it, you may see the final version of the app.

### Important Links

- Firebase - https://firebase.google.com/

- Postman Download - https://www.postman.com/downloads/

- Chrome Postman Extension - https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=en

- Postman API calls collection (You can use this url to import the same API calls) - https://www.getpostman.com/collections/68d33bf28ecf6dfbd1c8

- Firestore Get List Rest API Call - https://firestore.googleapis.com/v1/projects/my-awesome-nuxt-project/databases/(default)/documents/posts

### Commits

- Create a New Post From the Blog - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/e7f1c3fc5ca8201dd142f8bb2dc6fa3fbf74534b

- Adding UpdateDate and sorting to our Blog - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/b1db91abd724c62dbcd0a483b213e976382d6385

- Fetching Individual post - Replacing the hard coding with asyncData - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/eb084b931cdecf44672b7b578d5079f12d986cbb

- Assignment code - Use the selected post on Edit page - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/b2c6e949ae5cfa6a741268cafd59a33f31e4a615

- Edit Blog Post - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/a53762c4db0e80db4aa202d8737e51e8d8f093c5

- Fixing the Post sort after Edit - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/dc84946c42b702aaccad1f3c4371be644686dd38

## Section 6 - Config, Environment Variables, Transitions

Our Deployed Nuxt Blog - https://my-awesome-nuxt-blog.netlify.app/

> P.S - Our Deployed Nuxt Blog will have all the changes from the upcoming lectures too. So, when you see it, you may see the final version of the app.

## Section 7 - Real World Application - Continued

Our Deployed Nuxt Blog - https://my-awesome-nuxt-blog.netlify.app/

> P.S - Our Deployed Nuxt Blog will have all the changes from the upcoming lectures too. So, when you see it, you may see the final version of the app.

### Important Links

- Firebase - https://firebase.google.com/

- Nuxt JS Middleware - https://nuxtjs.org/api/pages-middleware/

- Firebase Signout example for Firebase sdk - https://firebase.google.com/docs/auth/web/password-auth#next_steps

- Firebase Security Rules - https://firebase.google.com/docs/rules/basics#development-environment_rules

### Commits

- Fixing Post Styles - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/b4359092f0a119110facd0d9ebb22c870e8ac535

- Adding the Filter for Timeago - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/9f03c280f7f7846b1b6e49238cf49eb42820b8bc

- Firebase Signup and Login Functionality - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/bf68a728a0b992f3941c3ef86fbd1c83d340c3ca

- Adding the Login and Logout link, storing token to vuex store - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/61044763ed77bbafc53cec542c1ea33c387ae409

- Implementing the Logout Functionality - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/180e1d056ed778fe0f5c35eaae6e1ce53e23676e

- Bug Fix for Default Vue Name - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/7a4966a1e94ed409797b0ae55bf4ca5f66d8e130

- Adding Security to our Application - Restrict Create/Edit post - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/d23570034c146d8b10632537c41e1d193d365fa8

- Keep the user Logged in the app - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/a99122489c692739d6018e3293306814849f8721

- Setting Expiration Date - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/4011ddf163a837a21f300018581efe8c1f8d6f3d

- Bug Fixes, Styling and Side Nav - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/7d4a09cba45f55fbe17f30ee5de787374550785f

- Using Cookies to maintain SSR Authentication - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/9352fb5ca57b1a702d6b5cb619c8a7380ef160ae

## Section 8 - Config, Environment Variables, Transitions

Our Deployed Nuxt Blog - https://my-awesome-nuxt-blog.netlify.app/

> P.S - Our Deployed Nuxt Blog will have all the changes from the upcoming lectures too. So, when you see it, you may see the final version of the app.

## Section 9 - Real World Application - Continued

Our Deployed Nuxt Blog - https://my-awesome-nuxt-blog.netlify.app/

> P.S - Our Deployed Nuxt Blog will have all the changes from the upcoming lectures too. So, when you see it, you may see the final version of the app.

### Important Links for this section

- Firebase - https://firebase.google.com/

- Nuxt JS Middleware - https://nuxtjs.org/api/pages-middleware/

- Firebase Signout example for Firebase sdk - https://firebase.google.com/docs/auth/web/password-auth#next_steps

- Firebase Security Rules - https://firebase.google.com/docs/rules/basics#development-environment_rules

### Commits

- Fixing Post Styles - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/b4359092f0a119110facd0d9ebb22c870e8ac535

- Adding the Filter for Timeago - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/9f03c280f7f7846b1b6e49238cf49eb42820b8bc

- Firebase Signup and Login Functionality - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/bf68a728a0b992f3941c3ef86fbd1c83d340c3ca

- Adding the Login and Logout link, storing token to vuex store - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/61044763ed77bbafc53cec542c1ea33c387ae409

- Implementing the Logout Functionality - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/180e1d056ed778fe0f5c35eaae6e1ce53e23676e

- Bug Fix for Default Vue Name - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/7a4966a1e94ed409797b0ae55bf4ca5f66d8e130

- Adding Security to our Application - Restrict Create/Edit post - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/d23570034c146d8b10632537c41e1d193d365fa8

- Keep the user Logged in the app - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/a99122489c692739d6018e3293306814849f8721

- Setting Expiration Date - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/4011ddf163a837a21f300018581efe8c1f8d6f3d

- Bug Fixes, Styling and Side Nav - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/7d4a09cba45f55fbe17f30ee5de787374550785f

- Using Cookies to maintain SSR Authentication - https://gitlab.com/abhay.talreja/my-awesome-nuxt-blog/-/commit/9352fb5ca57b1a702d6b5cb619c8a7380ef160ae

## Section 10 - SEO & PWA

Our Deployed Nuxt Blog - https://my-awesome-nuxt-blog.netlify.app/

### References:

- Final Demo Applicaiton - https://my-awesome-nuxt-blog.netlify.app/
- Nuxt JS - https://nuxtjs.org/
- VS Code - https://code.visualstudio.com/
- Node JS - https://nodejs.org/en/
- Npm JS - https://www.npmjs.com/
- Npx - https://www.npmjs.com/package/npx
- Netlify - http://netlfiy.com/
- Gitlab - http://gitlab.com/
- Nuxt Installation - https://nuxtjs.org/guide/installation
- Nuxt JS Docs: Routing - https://nuxtjs.org/guide/routing
- Google Fonts - https://fonts.google.com/
- Roboto Font - https://fonts.google.com/specimen/Roboto
- Vuex Guide - https://vuex.vuejs.org/guide/
- Vue Dev Tools - https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd?hl=en
- Firebase - https://firebase.google.com/
- Postman Download - https://www.postman.com/downloads/
- Chrome Postman Extension - https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=en
- Postman API calls collection (You can use this url to import the same API calls) - https://www.getpostman.com/collections/68d33bf28ecf6dfbd1c8
- Nuxt JS Middleware - https://nuxtjs.org/api/pages-middleware/
- Firebase Signout example for Firebase sdk - https://firebase.google.com/docs/auth/web/password-auth#next_steps
- Firebase Security Rules - https://firebase.google.com/docs/rules/basics#development-environment_rules


## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
